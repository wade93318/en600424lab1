from twisted.web import server, resource
from twisted.internet import reactor
from twisted.internet.protocol import Protocol
protocol='HTTP/1.0'
class Response_info(resource.Resource):
    isLeaf = True
    numberRequests = 0

    def response_GET(self, request):
        self.numberRequests += 1
        request.setHeader("content-type", 'text/html')
        self.send_response(request)
        return "Receiving..." + str(self.numberRequests) + "\n"
class echo(Protocol):
    def dataReceived(self, data):
        self.transport.write(data)
        self.transport.loseConnection()

reactor.listenTCP(8000, server.Site(Response_info()))
reactor.run()